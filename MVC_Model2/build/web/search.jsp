<%-- 
    Document   : search
    Created on : Dec 11, 2019, 10:31:16 AM
    Author     : HP
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:if test="${sessionScope.name==null}">
    <jsp:forward page="index.html"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Shopping!</title>
    </head>
    <body>
        <h1>Product List</h1>
        <form action="ProductFinder">
            <span style="color: red;">
                <c:out value="${param.msg}"/>
            </span>
            <input name="name"/><input type="submit"/>
            <a href="showAllPro.jsp">All Products</a><br>
        </form>
    </body>
</html>
