<%-- 
    Document   : login-success
    Created on : Dec 11, 2019, 10:08:04 AM
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            .tittle{margin: 10px auto;font-size: 20px;width: 320px;font-weight: bold}
            .tittlep{margin-top: 50px}
        </style>
    </head>
    <body>
        <form action="login-success.jsp" method="post">
            <p class="tittle tittlep"> You are successfully logged in!</p>
            <p class="tittle">
            <jsp:useBean id="bean" class="bean.LoginBean" scope="request"/>
            <h3>Welcome to the new world <a style='color: rgb(37,232,151);margin:center'><%=bean.getName()%></a>
            <%@include file="index.html" %>

        </form>
    </body>
</html>
