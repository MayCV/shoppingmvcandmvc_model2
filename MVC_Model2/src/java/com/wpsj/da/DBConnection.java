/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.da;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author HP
 */
public class DBConnection {
    private static Connection connection;
    public Connection getConnection() throws ClassNotFoundException, SQLException{
        if(connection==null){
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            connection = DriverManager.getConnection("jdbc:derby://localhost:1527/Products","sa","sa");
        }
        return connection;
    }
    public ResultSet getAccount(String username, String password) throws SQLException
    {
        ResultSet rs = null;
        Connection conn = DriverManager.getConnection("jdbc:derby://localhost:1527/FptAptech2019","sa","sa");
        Statement stm = conn.createStatement();
        String query = "SELECT * FROM user_account WHERE username='"+username+"' AND password='"+password+"' ";
        rs = stm.executeQuery(query);
        return rs;
    }
    
     public ResultSet checkLoginwithPreparaStatement(String username, String password) throws SQLException
    {
        ResultSet rs = null;
        PreparedStatement pstm = null;
        Connection conn = DriverManager.getConnection("jdbc:derby://localhost:1527/FptAptech2019","sa","sa");
        
        String query = "SELECT * FROM user_account WHERE username = ? AND password =? ";
        pstm = conn.prepareStatement(query);
        pstm.setString(1,username);
        pstm.setString(2,password);
        
        rs = pstm.executeQuery();
        return rs;
    }
}
