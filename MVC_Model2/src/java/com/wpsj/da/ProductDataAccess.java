/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.da;

import com.wpsj.entity.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author HP
 */
public class ProductDataAccess {
    private PreparedStatement statement;
    
    private PreparedStatement getSearchStatement() throws ClassNotFoundException,SQLException{
        if(statement == null){
            Connection connection = new DBConnection().getConnection();
            statement = connection.prepareStatement("SELECT pro_id, pro_name, pro_desc FROM ProductStore WHERE pro_name like ?");
            
        }
        return statement;
    }
    public List<Product> getProducts() throws SQLException, ClassNotFoundException{
        Connection con = new DBConnection().getConnection();
        PreparedStatement sta = con.prepareStatement("select * from ProductStore");
        ResultSet rs = sta.executeQuery();
        List<Product> products = new LinkedList<Product>();
        while(rs.next()){
            products.add(new Product(rs.getInt("pro_id"), rs.getString("pro_name"), rs.getString("pro_desc")));
        }
        return products;
    }
    public List<Product> getProductsByName(String name){
        try {
            PreparedStatement statement = getSearchStatement();
            statement.setString(1,"%"+name+"%");
            ResultSet rs = statement.executeQuery();
            List<Product> products = new LinkedList<Product>();
            while (rs.next()) {                
                products.add(new Product(rs.getInt("pro_id"),rs.getString("pro_name"),rs.getString("pro_desc")));
            }
            return products;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public void addProduct(Product p)throws ClassNotFoundException,SQLException{
            Connection connection = new DBConnection().getConnection();
            statement = connection.prepareStatement("INSERT INTO ProductStore(pro_id,pro_name,pro_desc) VALUES (?,?,?)");
            statement.setInt(1,p.getId());
            statement.setString(2,p.getName());
            statement.setString(3,p.getDesc());
            statement.execute();
            statement.close();
    }
    
    public void updateProduct(Product p)throws ClassNotFoundException,SQLException{
            Connection connection = new DBConnection().getConnection();
            statement = connection.prepareStatement("UPDATE ProductStore set pro_name=?,pro_desc=? WHERE pro_id=?");
            statement.setInt(1,p.getId());
            statement.setString(2,p.getName());
            statement.setString(3,p.getDesc());
            statement.executeUpdate();
            statement.close();
    }
    public void deleteProduct(int id)throws ClassNotFoundException,SQLException{
        Connection connection = new DBConnection().getConnection();
        statement = connection.prepareStatement("DELETE FROM ProductStore where pro_id=" + id);
        statement.execute();
    }
}
