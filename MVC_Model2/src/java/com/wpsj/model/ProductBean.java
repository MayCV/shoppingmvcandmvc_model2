/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.model;

import com.wpsj.da.ProductDataAccess;
import com.wpsj.entity.Product;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author HP
 */
public class ProductBean {
    public List<Product> getAllProduct() throws SQLException, ClassNotFoundException{
        return new ProductDataAccess().getProducts();
    }
}
