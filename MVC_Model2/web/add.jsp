<%-- 
    Document   : AddForm
    Created on : Dec 11, 2019, 11:41:22 AM
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Add Product!</h1>
        <a href="search.jsp">Search</a>
        <a href="showAllPro.jsp">All Products</a>
        <form action="AddPost">
            Id: <input type="text" name="pro_id"><br/>
            Name: <input type="text" name="pro_name"><br/>
            Description: <input type="text" name="pro_desc"><br/>
            <input type="submit" value="Add">
        </form>
    </body>
</html>
