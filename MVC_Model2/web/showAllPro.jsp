<%-- 
    Document   : showAllPro
    Created on : Dec 12, 2019, 8:01:13 PM
    Author     : HP
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:if test="${sessionScope.name==null}">
    <jsp:forward page="index.html"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h4>Hi, ${sessionScope.name}</h4>
        <h1>All Product!</h1>
        <a href="search.jsp">Search</a><br>
        <a href="AddProduct">Create New</a>
        <jsp:useBean class="com.wpsj.model.ProductBean" id="finder" scope="request"/>
        <table>
            <tr>
                <td style="font-weight: bold">ID</td>
                <td style="font-weight: bold">Name</td>
                <td style="font-weight: bold">Description</td>
                <td style="font-weight: bold">Action</td>
            </tr>
            <c:forEach items="${finder.allProduct}" var="product">
                <tr>
                    <td><c:out value="${product.id}"/></td>
                    <td><c:out value="${product.name}"/></td>
                    <td><c:out value="${product.desc}"/></td>
                    <td><a  href="UpdateProduct?id=${product.id}">Edit</a>
                        <a href="DeleteProduct?id=${product.id}">Delete</a></td>
                </tr>
            </c:forEach>
        </table>
        <a href="LogoutServlet">Logout</a>
    </body>
</html>
