<%-- 
    Document   : login-error
    Created on : Dec 11, 2019, 9:59:17 AM
    Author     : HP
--%>
<%@page import="bean.LoginBean" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="login-error.jsp" method="post">
            <p style="color: red;margin: 50px auto;font-size: 18px;width: 300px">Sorry! username or password error</p>
            <%@include file="index.html" %>
        </form>
    </body>
</html>
