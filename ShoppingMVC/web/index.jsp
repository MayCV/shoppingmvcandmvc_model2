<%--
    Document   : index
    Created on : Dec 13, 2019, 10:47:59 AM
    Author     : HP
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Order Product</title>
    </head>
    <body bgcolor="#FFFFCC">
        <h1>Welcome to Shop Stop!!!</h1>
        <hr/>
        <jsp:useBean id="prod" class="model.ProductCart" scope="session"/>
        <form action="ShoppingServlet" name="shoppingForm" method="post">
            <b>Products</b><br/>
            <select name="product">
                <c:forEach var="item" items="${prod.product}">
                    <option>
                        ${item.productId}${"|"}${item.productName}${"|"}${item.productType}${"|"}${item.price}
                    </option>
                </c:forEach>
            </select>
            <br/><br/>
            <b>Quantity</b> <input type="text" name="qty" value="1"><br/>
            <input type="hidden" name="action" value="ADD">
            <input type="submit" name="Submit" value="Add to Card">
        </form>
        <p>${message}</p>
        <c:if test="${prod.cartItems.size()!= 0}">
            <jsp:include page="cart.jsp" flush="true"/>
        </c:if>
    </body>
</html>
