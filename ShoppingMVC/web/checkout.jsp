<%-- 
    Document   : checkout
    Created on : Dec 13, 2019, 10:48:22 AM
    Author     : HP
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Transaction</title>
    </head>
    <body bgcolor="#FFFFcc">
        <h1>Transaction Details.</h1>
    <center>

        <table>
            <tr>
                <td><b> Product Id </b></td>
                <td><b> Product Name </b></td>
                <td><b> Product Type </b></td>
                <td><b> Price </b></td>
                <td><b> Quantity </b></td>
            </tr>
            <c:forEach var="item" items="${prod.cartItems}">
                <tr>
                    <td>${item.productId}</td>
                    <td>${item.productName}</td>
                    <td>${item.productType}</td>
                    <td>${item.price}</td>
                    <td>${item.quantity}</td>
                </tr>
            </c:forEach>
            <tr>
                <td></td>
                <td></td>
                <td><b> Total </b></td>
                <td><b> ${prod.amount} </b></td>
                <td></td>
            </tr>
        </table>
        <br>
        <a href="index.jsp"> Home </a>
        <br/>
        <br/>
        <br/>
        <table bgcolor="Lightgreen">
            <form action="AddUserBill" method="post">
                <tr>
                    <td></td>
                    <td><h3>BILL</h3></td>
                </tr>
                <tr>
                    <td><b>Name: </b></td>
                    <td><input type="text" name="name"</td>
                </tr>
                <tr>
                    <td><b>Age: </b></td>
                    <td><input type="text" name="age"</td>
                </tr>
                <tr>
                    <td><b>Address: </b></td>
                    <td><input type="text" name="address"</td>
                </tr>
                <tr>
                    <td><b>Email: </b></td>
                    <td><input type="email" name="email"</td>
                </tr>
                <tr>
                    <td><b>Phone Number: </b></td>
                    <td><input type="text" name="phonenumber"</td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="Order"</td>
                </tr>
            </form>
        </table>
    </center>
</body>
</html>
