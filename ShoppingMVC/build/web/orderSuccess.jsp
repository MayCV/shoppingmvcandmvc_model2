<%-- 
    Document   : orderSuccess
    Created on : Dec 16, 2019, 11:53:37 PM
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Thanks</title>
    </head>
    <body bgcolor="#FFFFcc">

    <center>
        <h3 style="color: green">Your order is being processed.<br/> Products will be delivered as soon as possible.<br/> Thanks for shopping! </h3>
        <a href="index.jsp" >Back to shopping</a>
    </center>
</body>
</html>
