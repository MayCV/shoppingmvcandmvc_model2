<%-- 
    Document   : shoppingError
    Created on : Dec 13, 2019, 10:48:50 AM
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Shopping</title>
    </head>
    <body>
        <h1>Error Occurred</h1>
        ${message}
        <a href="index.jsp">Home</a>
    </body>
</html>
