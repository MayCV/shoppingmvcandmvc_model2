/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author HP
 */
public class ProductCart {
    private final List cartItems;

    public ProductCart() {
       cartItems = new ArrayList();
    }
    
    private List products;


    public List getProduct(){
        List temp = new ArrayList();
        try {
            String dbUser = "sa";
            String dbPassword = "sa";
            String url = "jdbc:derby://localhost:1527/ShoppingCart";
            Connection con = DriverManager.getConnection(url, dbUser, dbPassword);
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery("select productId, productName, productType, price from ShoppingCart");
            while(rs.next()){
                Product item = new Product();
                item.setProductId(rs.getInt(1));
                item.setProductName(rs.getString(2));
                item.setProductType(rs.getString(3));
                item.setPrice(rs.getFloat(4));
                
                temp.add(item);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }//end of getProduct()
    

    public void addItem(int productId, String productName, String productType, float price, int quantity) {
        Product item = null;
        boolean match = false;
        for(int i=0; i < cartItems.size();i++){
            if(((Product) cartItems.get(i)).getProductId() == productId){
                item = (Product) cartItems.get(i);
                setAmount(getAmount()+ quantity * item.getPrice());
                item.setQuantity(item.getQuantity() + quantity);
                match = true;
                break;
            }
        }//end of for loop
        if(!match){
            item = new Product();
            item.setProductId(productId);
            item.setProductName(productName);
            item.setProductType(productType);
            item.setPrice(price);
            setAmount(getAmount() + quantity * item.getPrice());
            item.setQuantity(quantity);
            cartItems.add(item);
        }
    }

    public void removeItem(int productId) {
        for (int i = 0; i < cartItems.size(); i++) {
            Product item = (Product) cartItems.get(i);
            if(item.getProductId() == productId){
                setAmount(getAmount() - item.getPrice() * item.getQuantity());
                cartItems.remove(i);
                break;
            }
        }
    }
    
    public List getCartItems(){
        return cartItems;
    }
    
    private float amount;

    public float getAmount() {
        return this.amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    
    
}
