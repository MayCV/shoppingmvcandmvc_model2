/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;

/**
 *
 * @author HP
 */
//int code = (studentList.size() > 0) ? (studentList.size() + 1) : 1;
public class UserBill {
    int id;
    String name;
    int age;
    String address;
    String email;
    String phonenumber;

    public UserBill() {
    }
    
    public UserBill(int id, String name, int age, String address, String email, String phonenumber) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.address = address;
        this.email = email;
        this.phonenumber = phonenumber;
    }
    public List<UserBill> userBills;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        id = (userBills.size() > 0) ? (userBills.size() + 1) : 1;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }
    
    
}
