/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package da;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import model.UserBill;

/**
 *
 * @author HP
 */
public class DataAccess {
   private PreparedStatement statement;
   public void addUserBill(UserBill u)throws ClassNotFoundException,SQLException{
            Connection connection = new DBConnection().getConnection();
            statement = connection.prepareStatement("INSERT INTO UserBill(name,age,address,email,phonenumber) VALUES (?,?,?,?,?)");
            statement.setString(1,u.getName());
            statement.setInt(2,u.getAge());
            statement.setString(3,u.getAddress());
            statement.setString(4,u.getEmail());
            statement.setString(5,u.getPhonenumber());
            statement.execute();
            statement.close();
    }
}
